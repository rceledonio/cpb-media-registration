var express = require('express');
var router = express.Router();

router.get('/registraciones', (req, res) => {
  var str = '';
  var dataFile = req.app.get('appData');
  dataFile[2].data.forEach((item) => {
    str += `
    <tr>
      <td>${item.full_name}</td>
      <td>${item.phone}</td>
      <td>${item.email}</td>
      <td>${item.company}</td>
    </tr>
    `;
  })
  res.send(`
    <h2>Registraciones:</h2>
    <table>
    ${str}
    </table>
  `);
});

module.exports = router;
