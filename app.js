var express = require('express');
var app = express();
var dataFile = require('./data/luciovil_cpb.json');

app.get('/', (req, res) => {
  res.send('<h1>Hello</h1>');
});

app.set('port', process.env.PORT || 3000)
app.set('appData', dataFile);
//"full_name":"Amilcar Uriarte Garcia","idphoto_link":"uploads\/2017-03-04-07-58-52-049.jpg","photographer":"0","tv_reporter":"1","radio_reporter":"1","phone":"70757420","email":"amilcaruriarte@gmail.com","company":"RADIO RITMO - UNIVALLE TV","facebook_url":"https:\/\/www.facebook.com\/amilcarug","work_experience":"20 a&#241;os en Radiodifusi&#243;n\r\n18 A&#241;os presentador de Tv\r\n18 A&#241;os presentador de eventos culturales y otros.\r\nDocente Universitario\r\nConsultor Independiente\r\nEspecialista en Comunicaci&#243;n y Marketing","work_examples":"Redes Sociales\r\n","work_examples_link":"https:\/\/www.facebook.com\/amilcarug"},


app.use(require('./routes/registraciones'));




app.listen(app.get('port'), () => {
  console.log('Listening on port ' + app.get('port'));
});
